package ru.t1.karimov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.component.Bootstrap;
import ru.t1.karimov.tm.exception.AbstractException;

public final class Application {

    public static void main(@Nullable final String[] args) throws AbstractException {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
