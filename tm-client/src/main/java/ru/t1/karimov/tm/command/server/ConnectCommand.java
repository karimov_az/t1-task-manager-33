package ru.t1.karimov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.command.AbstractCommand;
import ru.t1.karimov.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().connect();
        @Nullable final Socket socket = getServiceLocator().getAuthEndpoint().getSocket();
        getServiceLocator().getDomainEndpoint().setSocket(socket);
        getServiceLocator().getProjectEndpoint().setSocket(socket);
        getServiceLocator().getTaskEndpoint().setSocket(socket);
        getServiceLocator().getUserEndpoint().setSocket(socket);
        getServiceLocator().getSystemEndpoint().setSocket(socket);
    }

    @NotNull
    @Override
    public String getName() {
        return "connect";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Connect to server.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }
}
