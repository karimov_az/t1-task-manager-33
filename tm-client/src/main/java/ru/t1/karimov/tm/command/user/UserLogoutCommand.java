package ru.t1.karimov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.karimov.tm.enumerated.Role;
import ru.t1.karimov.tm.exception.AbstractException;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public void execute() throws AbstractException {
        @NotNull UserLogoutRequest request = new UserLogoutRequest();
        getAuthEndpoint().logout(request);
        System.out.println("USER LOGOUT");
    }

    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User logout.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
