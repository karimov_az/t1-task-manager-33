package ru.t1.karimov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.domain.DataBase64LoadRequest;
import ru.t1.karimov.tm.enumerated.Role;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD BASE64]");
        getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from base64 file.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
