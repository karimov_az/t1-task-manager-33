package ru.t1.karimov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.karimov.tm.api.endpoint.*;
import ru.t1.karimov.tm.api.repository.ICommandRepository;
import ru.t1.karimov.tm.api.service.ICommandService;
import ru.t1.karimov.tm.api.service.ILoggerService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.IServiceLocator;
import ru.t1.karimov.tm.client.*;
import ru.t1.karimov.tm.command.AbstractCommand;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.karimov.tm.exception.system.CommandNotSupportedException;
import ru.t1.karimov.tm.repository.CommandRepository;
import ru.t1.karimov.tm.service.CommandService;
import ru.t1.karimov.tm.service.LoggerService;
import ru.t1.karimov.tm.service.PropertyService;
import ru.t1.karimov.tm.util.SystemUtil;
import ru.t1.karimov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.karimov.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ISystemEndpointClient systemEndpoint = new SystemEndpointClientClient();

    @Getter
    @NotNull
    private final ITaskEndpointClient taskEndpoint = new TaskEndpointClient();

    @Getter
    @NotNull
    private final IProjectEndpointClient projectEndpoint = new ProjectEndpointClient();

    @Getter
    @NotNull
    private final IAuthEndpointClient authEndpoint = new AuthEndpointClient();

    @Getter
    @NotNull
    private final IUserEndpointClient userEndpoint = new UserEndpointClient();

    @Getter
    @NotNull
    private final IDomainEndpointClient domainEndpoint = new DomainEndpointClient();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void processArguments(@Nullable final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.exit(0);
        } catch (@NotNull final Exception e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(@Nullable final String arg) throws AbstractException {
        @Nullable final AbstractCommand abstractArgument = commandService.getCommandByArgument(arg);
        if (abstractArgument == null) throw new ArgumentNotSupportedException(arg);
        abstractArgument.execute();
    }

    public void processCommand(@Nullable final String command) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    public void start(@Nullable final String[] args) throws AbstractException {
        processArguments(args);
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
