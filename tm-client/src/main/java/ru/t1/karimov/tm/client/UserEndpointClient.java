package ru.t1.karimov.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.endpoint.IUserEndpointClient;
import ru.t1.karimov.tm.dto.request.user.*;
import ru.t1.karimov.tm.dto.response.user.*;
import ru.t1.karimov.tm.exception.AbstractException;

public final class UserEndpointClient extends AbstractEndpointClient implements IUserEndpointClient {

    @Override
    public @NotNull UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request
    ) throws AbstractException {
        return call(request, UserChangePasswordResponse.class);
    }

    @Override
    public @NotNull UserLockResponse lockUser(@NotNull UserLockRequest request) throws AbstractException {
        return call(request, UserLockResponse.class);
    }

    @Override
    public @NotNull UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) throws AbstractException {
        return call(request, UserRegistryResponse.class);
    }

    @Override
    public @NotNull UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) throws AbstractException {
        return call(request, UserRemoveResponse.class);
    }

    @Override
    public @NotNull UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) throws AbstractException {
        return call(request, UserUnlockResponse.class);
    }

    @Override
    public @NotNull UserProfileResponse getUserProfile(@NotNull UserProfileRequest request) throws AbstractException {
        return call(request, UserProfileResponse.class);
    }

    @Override
    public @NotNull UserUpdateProfileResponse updateUserProfile(@NotNull UserUpdateProfileRequest request
    ) throws AbstractException {
        return call(request, UserUpdateProfileResponse.class);
    }

}
