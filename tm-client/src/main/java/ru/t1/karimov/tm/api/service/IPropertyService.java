package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getServerPort();

}
