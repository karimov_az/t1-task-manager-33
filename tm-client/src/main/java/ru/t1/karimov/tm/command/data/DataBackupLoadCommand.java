package ru.t1.karimov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.karimov.tm.dto.request.domain.DataBackupLoadRequest;
import ru.t1.karimov.tm.enumerated.Role;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().loadDataBackup(new DataBackupLoadRequest());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load backup from file.";
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
