package ru.t1.karimov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    void connect() throws IOException;

    void disconnect() throws IOException;

    @Nullable
    Socket getSocket();

    @Nullable
    void setSocket(Socket socket);

}
