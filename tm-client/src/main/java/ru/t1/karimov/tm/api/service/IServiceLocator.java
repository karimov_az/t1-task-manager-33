package ru.t1.karimov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainEndpointClient getDomainEndpoint();

    @NotNull
    IAuthEndpointClient getAuthEndpoint();

    @NotNull
    ISystemEndpointClient getSystemEndpoint();

    @NotNull
    ITaskEndpointClient getTaskEndpoint();

    @NotNull
    IProjectEndpointClient getProjectEndpoint();

    @NotNull
    IUserEndpointClient getUserEndpoint();

}
