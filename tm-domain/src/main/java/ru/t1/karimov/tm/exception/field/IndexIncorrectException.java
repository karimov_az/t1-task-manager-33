package ru.t1.karimov.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public final class IndexIncorrectException extends AbstractFieldException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

    public IndexIncorrectException(@NotNull String message) {
        super(message);
    }

}
