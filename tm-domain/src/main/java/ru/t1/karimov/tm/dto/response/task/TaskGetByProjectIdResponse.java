package ru.t1.karimov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.karimov.tm.dto.response.AbstractResponse;
import ru.t1.karimov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByProjectIdResponse extends AbstractResponse {

    @NotNull
    private List<Task> taskList;

    public TaskGetByProjectIdResponse(@NotNull final List<Task> taskList) {
        this.taskList = taskList;
    }

}
