package ru.t1.karimov.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.karimov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserProfileRequest extends AbstractUserRequest {
}
